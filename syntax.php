<?php
/**
 * DokuWiki Plugin dirpictures (Syntax Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  jerome <jerome_at_reso-nance.org>
 * 
 * Usage : 
 * ~~DIRPICTURES~~ : sort by name
 * ~~DIRPICTURES sortByDate~~ : sort by date
 * ~~DIRPICTURES sortByDate sortDesc~~ : sort by date (descendant)
 * 
 * Add default.jpg picture into the namespace of the page ...
 */

if(!defined('DOKU_INC')) {
    define ('DOKU_INC', realpath(dirname(__FILE__).'/../../').'/');
}
if(!defined('DOKU_PLUGIN')) {
    define ('DOKU_PLUGIN', DOKU_INC.'lib/plugins/');
}

require_once (DOKU_PLUGIN.'syntax.php');
require_once (DOKU_INC.'inc/search.php');

// Some functions
require_once (DOKU_PLUGIN.'dirpictures/sortPages.php');

class syntax_plugin_dirpictures extends DokuWiki_Syntax_Plugin {
	
	function getType() { return 'substition'; }
    function getPType() { return 'block'; }
    function getSort() { return 168; }
    
    // Some infos
    function getInfo() {
        return array(
            'author' => 'Jérôme Abel',
            'email'  => 'jerome_at_reso-nance.org',
            'date'   => '2016-05-31',
            'name'   => 'Dirpictures Plugin',
            'desc'   => 'Show subnamespaces with small pictures',
            'url'    => 'https://www.dokuwiki.org/plugin:dirpictures',
        );
    }
    
    // Pattern Dokuwiki
    function connectTo($mode) {
        $this->Lexer->addSpecialPattern('~~DIRPICTURES.*~~', $mode, 'plugin_dirpictures');
    }
    
    // Handle arguments
    function handle($match, $state, $pos, &$handler){
		//TODO : add arguments (namespace, depth, sort type, sort order, ...)

		// Get arguments
		$args = $match;
		$args = str_replace('DIRPICTURES', '', $args);
		$args = str_replace('~~', '', $args);
		$args = trim($args);
		$args = explode (' ', $args);
		
		// Prepare data options
		$data = array();
		$data['sort'] = "name";
		$data['order'] = "asc";
		
		// Set options
		foreach($args as $arg){
			$arg = trim($arg);
			if($arg == "sortByDate") {
				$data['sort'] = "date";
			}
			else if($arg == "sortDesc") {
				$data['order'] = "desc";
			}
		}
		
		// Send data options
		return $data;
	}
    
    // Render html
    function render($mode, &$renderer, $data) {		
		global $conf;
		global $ID;
		
		if($mode == 'xhtml'){
			
			// Search subnamespaces
			$searchOptions = array(
				'depth' => 1, 'listdirs'  => true, 'pagesonly'=> true,
				'sneakyacl' => true, 'meta'=> true, 'firsthead'=> true 
				);
			$files = array();
			$ns = getNS($ID);
			search($files, $conf['datadir'], 'search_universal', $searchOptions, $ns); 
		   	   
		    // Fill new array with data
		    // needed to sort by date
		    $new_files = array();
			$i=0;
			
			foreach($files as $item){
			   $id = $item['id'].":".$conf['start'];
			   $meta = p_get_metadata($id);
			   $picture = $meta['relation']['firstimage'];
			   if ($picture == "") $picture = $ns.":default.jpg"; // default image
			   $dim = array('w'=>350, 'h'=>220);
			   
			   //Useful data
			   $new_files[$i]['id'] = $id;
			   $new_files[$i]['date'] = $meta['date']['modified'];
			   $new_files[$i]['title'] = $meta['title'];
			   $new_files[$i]['img'] = ml($picture, $dim, true);
			   $new_files[$i]['url'] = wl($id,'',true);
			   		  
			   $i++;
		    }
		    
		    // Sort files with arguments
			sortPages::sort($new_files, $data['sort'], $data['order']);
		   
		    // Start html
			$renderer->doc .= '<div class="mosaicMain">';
						
			// Item loop
			foreach($new_files as $item){

				// Html test
				$renderer->doc .= '<a href="'.$item['url'].'" title="'.$item['title'].'">';
				$renderer->doc .= '<div class="mosaicImg" style="background-image:url('.$item['img'].')">';
				$renderer->doc .= '<span class="mosaicTitle">'.$item['title'];
				$renderer->doc .= '</span><span class="mosaicDate">' . date('d/m/Y', $item['date']);
				$renderer->doc .= '</span></div></a>'; 		
				}
				
			// End Html
			$renderer->doc .= '</div>';
			return true;
			} 	
		return false;
	 }
}
