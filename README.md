# Dirpictures Plugin for DokuWiki
Show namespaces with small pictures

- Version: 0.1
- Date: 31/05/2016
- Contact: jerome_at_reso-nance.org
- Website: https://www.dokuwiki.org/plugin:dirpictures
- Licence: GNU/GPL v3


## Usage
Choose one of these options and write it into a wiki page.

	// Sort by name
	~~DIRPICTURES~~

	// Sort by date (ascendant)
	~~DIRPICTURES sortByDate~~

	// Sort by date (descendant)
	~~DIRPICTURES sortByDate sortDesc~~


## Install
If you install this plugin manually, make sure it is installed in
lib/plugins/dirpictures/ - if the folder is called different it
will not work!

Please refer to http://www.dokuwiki.org/plugins for additional info
on how to install plugins in DokuWiki.


## Licence
CopyrightToLeft (Cto)) jerome <jerome_at_reso-nance.org>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See the COPYING file in your DokuWiki folder for details
