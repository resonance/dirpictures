<?php
/**
 * DokuWiki Plugin dirpictures (Syntax Component)
 *
 * @license GPL 2 http://www.gnu.org/licenses/gpl-2.0.html
 * @author  jerome <jerome_at_reso-nance.org>
 */
 
if(!defined('DOKU_INC')) die();

// Very basic sort functions
class sortPages {

    static function sort(&$files, $type, $order) {
       if ($type == "date") {
		   if ($order == "asc") usort($files, "sortPages::_dateAsc");
		   elseif ($order == "desc") usort($files, "sortPages::_dateDesc");
	   }
    }
    
    // Sort by Date Asc
    private static function _dateAsc($a, $b) {
		if ($a['date'] == $b['date']) return 0;
		return ($a['date'] < $b['date']) ? -1 : 1;
	}
	
	// Sort by Date Desc
	private static function _dateDesc($a, $b) {
		if ($a['date'] == $b['date']) return 0;
		return ($a['date'] > $b['date']) ? -1 : 1;
	}
}

